package GameOfLife;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

/**
 * Implements the world for the Game Of Life 
 * Calculates the next state of the world, based on its current state
 * The world is treated as a toroidal array
 * 
 * @author Csaba Petr�s
 *
 */
public class World {
	
	/**
	 * The current state of the world
	 */
	private JsonArray currentCells;
	
	/**
	 * The next state of the world
	 */
	private int[][] nextCells;
	
	/**
	 * Width of the world
	 */
	private int worldWidth;
	
	/**
	 * Height of the world
	 */
	private int worldHeight;
	
	/**
	 * Sets the current state of the world
	 * 
	 * @param cells
	 */
	public void setCurrentCells (JsonArray cells) {
		currentCells = cells;
	}
	
	/**
	 * Sets the width of the world
	 * 
	 * @param worldWidth
	 */
	public void setWorldWidth(int worldWidth) {
		this.worldWidth = worldWidth;
	}
	
	/**
	 * Sets the height of the world
	 * 
	 * @param worldHeight
	 */
	public void setWorldHeight(int worldHeight) {
		this.worldHeight = worldHeight;
	}
	
	/**
	 * Initializes the array with the world size to store the next state
	 */
	public void initNextCells() {
		this.nextCells = new int[worldWidth][worldHeight];
	}
	
	/**
	 * Iterates through the current state of the world and sets the next state
	 */
	public void tick() {
		for (int x = 0; x < worldWidth; x++) {
			for (int y = 0; y < worldHeight; y++) {
				setNextCellState(x, y);
			}
		}
	}
	
	/**
	 * Returns the next state of the world as JSON
	 */
	public String toString() {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		for (int x = 0; x < worldWidth; x++) {
			JsonArrayBuilder rowBuilder = Json.createArrayBuilder();
			for (int y = 0; y < worldHeight; y++) {
				rowBuilder.add(nextCells[x][y]);
			}
			JsonArray row = rowBuilder.build();
			builder.add(row);
		}
		JsonArray world = builder.build();
		return world.toString();
	}
	
	/**
	 * Sets the next state of the cell on the given coordinates
	 * 
	 * @param x
	 * @param y
	 */
	private void setNextCellState(int x, int y) {
		int currentState = getCurrentCellState(x, y);
		int nextState = currentState;
		int sum = calculateNeighbourSum(x, y);
		if (currentState == 1) {
			if (sum < 2 || sum > 3) {
				nextState = 0;
			}
		} else {
			if (sum == 3) {
				nextState = 1;
			}
		}
		nextCells[x][y] = nextState;
	}
	
	/**
	 * Returns the current state of a given cell on the given coordinates
	 * @param x
	 * @param y
	 * @return int The current state of the cell on the given coordinates
	 */
	private int getCurrentCellState(int x, int y) {
		JsonArray row = currentCells.getJsonArray(x);
		return row.getInt(y);
	}
	
	/**
	 * Returns the X coordinate of the cell left to the given X coordinate in a toroidal array
	 * 
	 * @param x
	 * @return int The X coordinate of the adjacent cell
	 */
	private int leftNeighbourX(int x) {
		int neighbourX = x - 1;
		if (neighbourX < 0) {
			neighbourX = worldWidth - 1;
		}
		return neighbourX;
	}
	
	/**
	 * Returns the X coordinate of the cell right to the given X coordinate in a toroidal array
	 * 
	 * @param x
	 * @return int The X coordinate of the adjacent cell
	 */
	private int rightNeighbourX(int x) {
		int neighbourX = x + 1;
		if (neighbourX > worldWidth - 1) {
			neighbourX = 0;
		}
		return neighbourX;
	}
	
	/**
	 * Returns the Y coordinate of the cell above the given Y coordinate in a toroidal array
	 * 
	 * @param y
	 * @return in The Y coordinate of the adjacent cell
	 */
	private int topNeighbourY(int y) {
		int neighbourY = y - 1;
		if (neighbourY < 0) {
			neighbourY = worldHeight - 1;
		}
		return neighbourY;
	}
	
	/**
	 * Returns the Y coordinate of the cell below the given Y coordinate in a toroidal array 
	 * 
	 * @param y
	 * @return int The Y coordinate of the adjacent cell
	 */
	private int bottomNeighbourY(int y) {
		int neighbourY = y + 1;
		if (neighbourY > worldHeight - 1) {
			neighbourY = 0;
		}
		return neighbourY;
	}
	
	/**
	 * Returns the coordinates of the top-left adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getTopLeftNeighbourState(int x, int y) {
		x = leftNeighbourX(x);
		y = topNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the top-center adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getTopCenterNeighbourState(int x, int y) {
		y = topNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the top-right adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getTopRightNeighbourState(int x, int y) {
		x = rightNeighbourX(x);
		y = topNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the middle-right adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getMiddleRightNeighbourState(int x, int y) {
		x = rightNeighbourX(x);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the bottom-right adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getBottomRightNeighbourState(int x, int y) {
		x = rightNeighbourX(x);
		y = bottomNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the bottom-center adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getBottomCenterNeighbourState(int x, int y) {
		y = bottomNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the coordinates of the bottom-left adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getBottomLeftNeighbourState(int x, int y) {
		x = leftNeighbourX(x);
		y = bottomNeighbourY(y);
		return getCurrentCellState(x, y);
	}
	
	
	/**
	 * Returns the coordinates of the middle-left adjacent cell of the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The coordinates of the adjacent cell in a toroidal array
	 */
	private int getMiddleLeftNeighbourState(int x, int y) {
		x = leftNeighbourX(x);
		return getCurrentCellState(x, y);
	}
	
	/**
	 * Returns the sum of the states of all adjacent cells for the given cell
	 * 
	 * @param x
	 * @param y
	 * @return int The sum of the states of all adjacent cells
	 */
	private int calculateNeighbourSum(int x, int y) {
		int sum = getTopLeftNeighbourState(x, y)
			+ getTopCenterNeighbourState(x, y)
			+ getTopRightNeighbourState(x, y)
			+ getMiddleRightNeighbourState(x, y)
			+ getBottomRightNeighbourState(x, y)
			+ getBottomCenterNeighbourState(x, y)
			+ getBottomLeftNeighbourState(x, y)
			+ getMiddleLeftNeighbourState(x, y);
		return sum;
	}	
}
