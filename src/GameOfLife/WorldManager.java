package GameOfLife;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Creates a world instance for the Game Of Life
 * 
 * @author Csaba Petr�s
 *
 */
public class WorldManager {
	
	/**
	 * Returns a World instance from the given JSON parameters
	 * @param jsonParams
	 * @return World An instance of the World of Game Of Life
	 */
	public World parseWorld(String jsonParams) {
		World world = new World();
		JsonReader jsonReader = Json.createReader(new StringReader(jsonParams));
	    JsonObject params = jsonReader.readObject();
	    jsonReader.close();
	    world.setCurrentCells(params.getJsonArray("cells"));
	    world.setWorldWidth(params.getInt("worldWidth"));
	    world.setWorldHeight(params.getInt("worldHeight"));
	    world.initNextCells();
		return world;
	}
}
