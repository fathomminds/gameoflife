package GameOfLife;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.ModelMap;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/world")
public class GameOfLifeController {
   
	@RequestMapping(method = RequestMethod.GET)
	public String printHello(ModelMap model) {
      model.addAttribute("worldWidth", 5);
      model.addAttribute("worldHeight", 5);
      return "world";
   }
	
	@RequestMapping(
	  value = "/tick", 
	  method = RequestMethod.POST, 
	  produces = "application/json"
	)
	@ResponseBody
	public ResponseEntity<String> tick(HttpServletRequest request) {
		String jsonParams = request.getParameter("params");
		World world = new WorldManager().parseWorld(jsonParams);
		world.tick();
        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<String>(world.toString(), httpHeaders, HttpStatus.OK);
	}
}
