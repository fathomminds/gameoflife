<%@ page contentType = "text/html; charset = UTF-8" %>
<html>
   <head>
      <title>Game Of Life</title>
      <style type="text/css">
      #world {
      	margin: auto;
      	margin-top: 100px;
      	border-collapse: collapse;
      }
      #world td {
      	border: 1px solid #000;
      	cursor: pointer;
      	width: 30px;
      	height: 30px;
      }
      #world td.live {
      	background: #000;
      }
      #world td.dead {
      	background: #fff;
      }
      </style>
   </head>
   <body>
   	  <div>
   	    <span style='display: inline-block; width: 150px; padding-right: 5px;'>Width:</span><input onchange="app.worldWidth=parseInt(this.value); app.initWorld(); app.drawWorld();" type="range" id="worldWidth" min="3" max="20" value="${worldWidth}" /><br />
   	    <span style='display: inline-block; width: 150px; padding-right: 5px;'>Height:</span><input onchange="app.worldHeight=parseInt(this.value); app.initWorld(); app.drawWorld();" type="range" id="worldHeight" min="3" max="20" value="${worldHeight}" />
   	  </div>
   	  <button id='startSimulation' onclick="app.startSimulation();">Start</button>
   	  <button id='stopSimulation' onclick="app.stopSimulation();">Stop</button>
   	  <button id='clearWorld' onclick="app.clearWorld();">Clear</button>
   	  <button id='randomize' onclick="app.initWorld(app.worldWidth, app.worldHeight); app.redraw();">Randomize</button><br />
   	  <br />
   	  <div>
   	  	<i>Hint: clicking on the cells toggle their state</i>
   	  </div>
   	  <br />
      <table id="world"></table>
      <script>
      (function(global){
    	  "use strict";
    	  
    	  global.app = {
    		running: false,
    		timer: 0,
    		tickInterval: 500,
    		worldWidth: ${worldWidth},
    		worldHeight: ${worldHeight},
    		cells: [],
    		initWorld (width, height) {
      		  this.cells = [];
      		  for (var x = 0; x < this.worldWidth; x++) {
      			  this.cells[x] = [];
      			  for (var y = 0; y < this.worldHeight; y++) {
      				  this.cells[x][y] = Math.random() >= 0.5 ? 1 : 0;
      			  }
      		  }
      	  	},
    		drawWorld: function () {
      		  var table = document.getElementById("world");
      		  table.innerHTML = "";
      		  for (var currentRow = 0; currentRow < this.worldHeight; currentRow++) {
      			  var row = document.createElement('tr');
      			  for (var currentCol = 0; currentCol < this.worldWidth; currentCol++) {
      				  var cell = document.createElement('td');
      				  cell.className = this.cells[currentCol][currentRow] === 1 ? 'live' : 'dead';
      				  cell.setAttribute('data-x', currentCol);
      				  cell.setAttribute('data-y', currentRow);
      				  cell.id = 'cell-' + currentCol + '-' + currentRow;
      				  cell.onclick = function() {
      					  if (app.running) return;
      					  this.className = this.className === 'dead' ? 'live' : 'dead';
      					  var x = this.getAttribute('data-x');
      					  var y = this.getAttribute('data-y');
      					  app.cells[x][y] = this.className === 'live' ? 1 : 0;
      				  }
      				  row.appendChild(cell);
      			  }
      			  table.appendChild(row);
      		  }
      	  	},
      	  	redraw: function () {
      	  		for (var currentCol = 0; currentCol < this.worldWidth; currentCol++) {
      	  			for (var currentRow = 0; currentRow < this.worldHeight; currentRow++) {
      	  				var cell = document.getElementById('cell-' + currentCol + '-' + currentRow);
      	  				cell.className = this.cells[currentCol][currentRow] === 1 ? 'live' : 'dead';
      	  			}
      	  		}	
      	  	},
    		startSimulation: function () {
    			this.running = true;
    			this.tick();
    		},
    		stopSimulation: function () {
    			this.running = false;
    			clearTimeout(this.timer);
    		},
    		clearWorld: function () {
    			this.cells = [];
        		  for (var x = 0; x < this.worldWidth; x++) {
        			  this.cells[x] = [];
        			  for (var y = 0; y < this.worldHeight; y++) {
        				  this.cells[x][y] = 0;
        			  }
        		  }
        		this.redraw();
    		},
    		tick: function() {
    			var xhr = new XMLHttpRequest();
    			xhr.open('POST', 'world/tick');
    			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    			xhr.onload = function() {
    			    if (xhr.status === 200) {
    			    	app.cells = JSON.parse(xhr.responseText);
    			        app.redraw();
    			    }
    			    else if (xhr.status !== 200) {
    			        console.log('Request failed.  Returned status of ' + xhr.status);
    			    }
    			};
    			var params = {};
    			params.worldWidth = this.worldWidth;
    			params.worldHeight = this.worldHeight;
    			params.cells = this.cells;
    			xhr.send(encodeURI('params=' + JSON.stringify(params)));
    			this.timer = setTimeout(this.tick.bind(this), this.tickInterval);
    		}
    	  }
    	  global.app.initWorld();
    	  global.app.drawWorld();
      })(window);
      </script>
   </body>
</html>